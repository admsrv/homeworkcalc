﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Manager.cs" company="Serhiy Chepur">
//   2014. S.Chepur@MidlleGroup
// </copyright>
// <summary>
//   Defines the Manager type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using HomeWorkCalc.LectionThree.TaskOne;

namespace HomeWorkCalc.Base
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using HomeWorkCalc.TaskOne;
    using HomeWorkCalc.TaskThree;

    /// <summary>
    /// The manager.
    /// </summary>
    public class Manager
    {
        /// <summary>
        /// The expression string.
        /// </summary>
        private readonly string expressionString;

        /// <summary>
        /// The work mode.
        /// </summary>
        private readonly Client workMode;

        /// <summary>
        /// Initializes a new instance of the <see cref="Manager"/> class.
        /// </summary>
        /// <param name="incomingExpression">
        /// The incoming expression.
        /// </param>
        /// <param name="workMode">
        /// The work mode.
        /// </param>
        public Manager(string incomingExpression, Client workMode)
        {
            this.expressionString = incomingExpression;
            this.workMode = workMode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Manager"/> class.
        /// </summary>
        public Manager()
        {}

        /// <summary>
        /// The do persons demo. #Lecture3
        /// </summary>
        public void DoPersonsDemo()
        {
            bool doPersonDemoFlag = true;
            string tmpInput = string.Empty;
            Person[] personsArray = new Person[0];
            do
            {
                Console.Clear();
                Console.WriteLine("1:\t Fill teachers array.\n" + 
                                  "2:\t Fill students array.\n" +
                                  "3:\t Print list of persons.\n"
                                  + "Q:\t Quit.\n");
                var input = Console.ReadLine();
                switch (input)
                {
                    case "1":
                    {
                        ConGetTeachers(ref personsArray);
                        break;
                    }

                    case "2":
                    {
                        ConGetStudents(ref personsArray);
                        break;
                    }

                    case "3":
                    {
                        Console.Clear();
                        foreach (Person persona in personsArray)
                        {
                            Console.WriteLine(persona.ToString());
                        }

                        Console.ReadLine();
                        break;
                    }

                    case "4":
                        {
                            GetPersonsArrayInfo(personsArray);
                            Console.ReadLine();
                            break;
                        }

                    case "Q": 
                        doPersonDemoFlag = false;
                        break;
                }
            }
            while (doPersonDemoFlag);
        }

        private static void GetPersonsArrayInfo(Person[] personArray)
        {
            if (personArray != null)
            {
                foreach (var personaObject in personArray)
                {
                    Console.WriteLine(personaObject.GetHashCode());
                }
            }
        }

        /// <summary>
        /// The con get students.
        /// </summary>
        /// <param name="personsArray">
        /// The persons array.
        /// </param>
        private static void ConGetStudents(ref Person[] personsArray)
        {
            Console.Clear();
            Console.WriteLine("\t Enter a student info at next format: Firstname Surname LastName\n" +
                              "\t for stop add students press \'Enter\' in empty string.\n");
            string tmpInput;
            while (!string.IsNullOrEmpty(tmpInput = Console.ReadLine()))
            {
                string[] fio;
                if (!GetPersonInitals(tmpInput, out fio))
                {
                    continue;
                }

                Array.Resize(ref personsArray, personsArray.Length + 1);
                personsArray[personsArray.Length - 1] = new Student(fio);
                ConGetTeachersByStudent(ref personsArray[personsArray.Length - 1]);
            }

            Console.WriteLine("Leave students list fill");
        }

        /// <summary>
        /// The con get teachers by student.
        /// </summary>
        /// <param name="studentObj">
        /// The student object.
        /// </param>
        private static void ConGetTeachersByStudent(ref Person studentObj)
        {
            Console.Clear();
            Console.WriteLine("\t Fill a teachers list for {0}, at next format: Firstname Surname LastName\n" +
                              "\t for stop add teachers press \'Enter\' in empty string.\n", studentObj.ToString());
            string tmpInput;
            while (!string.IsNullOrEmpty(tmpInput = Console.ReadLine()))
            {
                string[] fio;
                if (!GetPersonInitals(tmpInput, out fio))
                {
                    continue;
                }

                (studentObj as Student).Teachers.Add(new Teacher(fio));
            }

            Console.WriteLine("Leave students list fill");
        }

        /// <summary>
        /// The console method 4 get teachers array.
        /// </summary>
        /// <param name="personsArray">
        /// The persons array.
        /// </param>
        private static void ConGetTeachers(ref Person[] personsArray)
        {
            Console.Clear();
            Console.WriteLine("\t Enter a teacher info at next format: Firstname Surname LastName\n" +
                              "\t for stop add teachers press \'Enter\' in empty string.\n");
            string tmpInput;
            while (!string.IsNullOrEmpty(tmpInput = Console.ReadLine()))
            {
                string[] fio;
                if (!GetPersonInitals(tmpInput, out fio))
                {
                    continue;
                }

                Array.Resize(ref personsArray, personsArray.Length + 1);
                personsArray[personsArray.Length - 1] = new Teacher(fio);
                ConGetStudentsByTeacher(ref personsArray[personsArray.Length - 1]);
            }

            Console.WriteLine("Leave teachers list fill");
        }

        /// <summary>
        /// The get person initals.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <param name="fio">
        /// The fio.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool GetPersonInitals(string input, out string[] fio)
        {
            fio = input.Split(new char[1] { ' ' });
            return fio.Length == 3;
        }

        /// <summary>
        /// The console get students by teacher.
        /// </summary>
        /// <param name="teacherObj">
        /// The teacher object.
        /// </param>
        private static void ConGetStudentsByTeacher(ref Person teacherObj)
        {
            Console.Clear();
            Console.WriteLine("\t Fill a students list for {0}, at next format: Firstname Surname LastName\n" +
                              "\t for stop add students press \'Enter\' in empty string.\n", teacherObj.ToString());
            string tmpInput;
            while (!string.IsNullOrEmpty(tmpInput = Console.ReadLine()))
            {
                string[] fio;
                if (!GetPersonInitals(tmpInput, out fio))
                {
                    continue;
                }

                (teacherObj as Teacher).Students.Add(new Student(fio));
            }

            Console.WriteLine("Leave students list fill");
        }

        /// <summary>
        /// The do real numbers parse from strings. #task3.
        /// </summary>
        public void DoRealNumbers()
        {
            try
            {
                switch (this.workMode)
                {
                    case Client.Console:
                        {
                            bool read = true;
                            var worker = new List<string>();
                            while (read)
                            {
                                var tmpStr = Console.ReadLine();
                                if (tmpStr == "0")
                                {
                                    read = false;
                                }
                                else
                                {
                                    worker.Add(tmpStr);
                                }
                            }

                            var performToDoubleStrings = new TaskThree(worker.ToArray());
                            Console.WriteLine(performToDoubleStrings.PerformedArrayToString);
                            break;
                        }
                }
            }
            catch (Exception programExceprion)
            {
                switch (this.workMode)
                {
                    case Client.Console:
                        {
                            Console.WriteLine(programExceprion.Message);
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// The convert to upper first chars. #task4
        /// </summary>
        public void DoConvertToUpperFirstChars()
        {
            try
            {
                switch (this.workMode)
                {
                    case Client.Console:
                        {
                            var inputWords = Console.ReadLine();
                            var firstCharsToUpper = new TaskFour.TaskFour(inputWords);
                            Console.WriteLine(firstCharsToUpper.PerformedString);
                            break;
                        }
                }
            }
            catch (Exception programExceprion)
            {
                switch (this.workMode)
                {
                    case Client.Console:
                        {
                            Console.WriteLine(programExceprion.Message);
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// The get bytes from string.#task5
        /// </summary>
        public void DoGetBytesFromString()
        {
            try
            {
                switch (this.workMode)
                {
                    case Client.Console:
                        {
                            var inputString = Console.ReadLine();
                            var stringCharsToBytes = new TaskFive.TaskFive(inputString, Console.InputEncoding);
                            Console.WriteLine(stringCharsToBytes.StringBytes);
                            break;
                        }
                }
            }
            catch (Exception programExceprion)
            {
                switch (this.workMode)
                {
                    case Client.Console:
                        {
                            Console.WriteLine(programExceprion.Message);
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// The do calc.
        /// </summary>
        public void DoCalc()
        {
            try
            {
                switch (this.workMode)
                {
                    case Client.Console:
                        {
                            var workerTokens = this.PrepareString(this.expressionString);
                            var equationWorker = new ExpressionReader(workerTokens);
                            var equationValue = equationWorker.MathExpression;
                            Console.WriteLine(equationValue.Init());
                            break;
                        }
                }
            }
            catch (Exception programExceprion)
            {
                switch (this.workMode)
                {
                    case Client.Console:
                        {
                            Console.WriteLine(programExceprion.Message);
                            break;
                        }
                }
            }
        }


        /// <summary>
        /// The prepare string.
        /// </summary>
        /// <param name="unpreparedString">
        /// The unprepared string.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        private List<string> PrepareString(string unpreparedString)
        {
            var preparedTokens = new List<string> { };
            if (!string.IsNullOrEmpty(unpreparedString))
            {
              var preparedExpressionString = this.expressionString.ToLower();
              var abc = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', '\'', '"', '.' };
              preparedExpressionString = preparedExpressionString.Trim(abc);
              preparedTokens = new List<string>(preparedExpressionString.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
            }

            return preparedTokens;
        }
    }
}
