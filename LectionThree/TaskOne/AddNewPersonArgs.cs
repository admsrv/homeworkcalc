// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AddNewPersonArgs.cs" company="Serhiy Chepur">
//   2014. Serhiy Chepur@MidlleGroup
// </copyright>
// <summary>
//   The add new person args.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.LectionThree.TaskOne
{
    using System;

    /// <summary>
    /// The add new person args.
    /// </summary>
    public class AddNewPersonArgs : EventArgs
    {
        /// <summary>
        /// The deny to add.
        /// </summary>
        private string personaLastName;

        /// <summary>
        /// The is access denieded.
        /// </summary>
        private bool isAccessDenieded;

        /// <summary>
        /// Initializes a new instance of the <see cref="AddNewPersonArgs"/> class.
        /// </summary>
        /// <param name="lastName">
        /// The last name.
        /// </param>
        /// <param name="isAccessDenieded">
        /// Is the Access Denieded.
        /// </param>
        public AddNewPersonArgs(string lastName, bool isAccessDenieded = false)
        {
            this.personaLastName = lastName;
            this.isAccessDenieded = isAccessDenieded;
        }

        /// <summary>
        /// Gets or sets the student last name.
        /// </summary>
        public string PersonaLastName
        {
            get
            {
                return this.personaLastName;
            }

            set
            {
                this.personaLastName = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is person add denieded.
        /// </summary>
        public bool IsPersonAddDenieded
        {
            get
            {
                return this.isAccessDenieded;
            }

            set
            {
                this.isAccessDenieded = value;
            }
        }
    }
}