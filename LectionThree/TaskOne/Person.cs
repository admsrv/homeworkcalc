﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Person.cs" company="Serhiy Chepur">
//   2014. Serhiy Chepur@MidlleGroup
// </copyright>
// <summary>
//   The person.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.LectionThree.TaskOne
{
    /// <summary>
    /// The fio.
    /// </summary>
    /// <param name="firstName">
    /// The first name.
    /// </param>
    /// <param name="secondName">
    /// The second name.
    /// </param>
    /// <param name="lastName">
    /// The last name.
    /// </param>
    /// <returns>
    /// The <see cref="string"/>.
    /// </returns>
    internal delegate string Fio(string firstName, string secondName, string lastName); 

    /// <summary>
    /// The person.
    /// </summary>
    public class Person
    {
        /// <summary>
        /// The first name.
        /// </summary>
        private readonly string firstName;

        /// <summary>
        /// The second name.
        /// </summary>
        private readonly string secondName;

        /// <summary>
        /// The last name.
        /// </summary>
        private readonly string lastName;

        /// <summary>
        /// Initializes a new instance of the <see cref="Person"/> class.
        /// </summary>
        /// <param name="firstName">
        /// The first name.
        /// </param>
        /// <param name="secondName">
        /// The second name.
        /// </param>
        /// <param name="lastName">
        /// The last name.
        /// </param>
        public Person(string firstName, string secondName, string lastName)
        {
            this.firstName = firstName;
            this.secondName = secondName;
            this.lastName = lastName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Person"/> class.
        /// </summary>
        /// <param name="personInfo">
        /// The person info.
        /// </param>
        public Person(string[] personInfo)
        {
            this.firstName = personInfo[0];
            this.secondName = personInfo[1];
            this.lastName = personInfo[2];
        }

        /// <summary>
        /// Gets the first name.
        /// </summary>
        public string FirstName
        {
            get
            {
                return this.firstName;
            }
        }

        /// <summary>
        /// Gets the second name.
        /// </summary>
        public string SecondName
        {
            get
            {
                return this.secondName;
            }
        }

        /// <summary>
        /// Gets the last name.
        /// </summary>
        public string LastName
        {
            get
            {
                return this.lastName;
            }
        }


        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            Fio slovianInitials = (name, secName, lstName) => string.Concat(lstName, " ", name, " ", secName);
            return slovianInitials(this.firstName, this.secondName, this.lastName); 
        }
    }
}
