// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PersonAddFilter.cs" company="Serhiy Chepur">
//   2014. Serhiy Chepur@MidlleGroup
// </copyright>
// <summary>
//   The PersonList filter class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.LectionThree.TaskOne
{
    /// <summary>
    /// The PersonList filter class.
    /// </summary>
    public class PersonAddFilter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonAddFilter"/> class.
        /// </summary>
        /// <param name="personList">
        /// The person list.
        /// </param>
        /// <param name="deniededName">
        /// The denieded name.
        /// </param>
        public PersonAddFilter(PersonList personList, string deniededName)
        {
            personList.RaiseAddPersonaEvent += delegate(object sender, AddNewPersonArgs args)
            {
                if (args.PersonaLastName == deniededName)
                {
                    args.IsPersonAddDenieded = true;
                }
            };
        }
    }
}