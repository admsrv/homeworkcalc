﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PersonList.cs" company="Serhiy Chepur">
//   2014. Serhiy Chepur@MidlleGroup
// </copyright>
// <summary>
//   Defines the PersonList type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.LectionThree.TaskOne
{
    using System;
    using System.Collections;

    /// <summary>
    /// The student list.
    /// </summary>
    public class PersonList : IEnumerable, IEnumerator
    {
        /// <summary>
        /// The persons.
        /// </summary>
        private Person[] persons;

        /// <summary>
        /// The position.
        /// </summary>
        private int position = -1;

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonList"/> class.
        /// </summary>
        /// <param name="persons">
        /// The persons.
        /// </param>
        public PersonList(Person[] persons)
        {
            this.persons = persons;
        }

        /// <summary>
        /// The raise add persona event.
        /// </summary>
        public event EventHandler<AddNewPersonArgs> RaiseAddPersonaEvent;

        /// <summary>
        /// Gets the current.
        /// </summary>
        public object Current
        {
            get
            {
                try
                {
                    return this.persons[this.position];
                }
                catch (Exception)
                {
                    throw new Exception("Students list: Cant get current");
                }
            }
        }

        /// <summary>
        /// Gets the length.
        /// </summary>
        public int Length
        {
            get
            {
                return this.persons.Length;
            }
        }

        /// <summary>
        /// The this.
        /// </summary>
        /// <param name="personaIndex">
        /// The persona index.
        /// </param>
        /// <returns>
        /// The <see cref="Person"/>.
        /// </returns>
        public Person this[int personaIndex]
        {
            get
            {
                return this.persons[personaIndex];
            }
        }

       /// <summary>
        /// The get enumerator.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerator"/>.
        /// </returns>
        public IEnumerator GetEnumerator()
        {
            return this;
        }

        /// <summary>
        /// The add new student.
        /// </summary>
        /// <param name="newStudent">
        /// The new student.
        /// </param>
        public void Add(Person newStudent)
        {
            var studentAddEvent = new AddNewPersonArgs(newStudent.LastName);
            switch (this.OnRaiseAddPersonEvent(studentAddEvent))
            {
                case false:
                    {
                        Array.Resize(ref this.persons, this.persons.Length + 1);
                        this.persons[this.persons.Length - 1] = newStudent;
                        break;
                    }
            }
        }

        /// <summary>
        /// The move next.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool MoveNext()
        {
            this.position++;
            return this.position < this.persons.Length;
        }

        /// <summary>
        /// The reset.
        /// </summary>
        public void Reset()
        {
            this.position = -1;
        }

        /// <summary>
        /// The on raise add person event.
        /// </summary>
        /// <param name="eventArgs">
        /// The event args.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool OnRaiseAddPersonEvent(AddNewPersonArgs eventArgs)
        {
            var answer = true;
            var handler = this.RaiseAddPersonaEvent;
            if (handler != null)
            {
                handler(this, eventArgs);
                answer = eventArgs.IsPersonAddDenieded;
            }

            return answer;
        }
    }
}