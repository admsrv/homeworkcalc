﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Student.cs" company="Serhiy Chepur">
//   2014. Serhiy Chepur@MidlleGroup
// </copyright>
// <summary>
//   Defines the Student type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.LectionThree.TaskOne
{
    /// <summary>
    /// The student.
    /// </summary>
    public class Student : Person
    {
        /// <summary>
        /// The first name.
        /// </summary>
        private readonly string firstName;

        /// <summary>
        /// The second name.
        /// </summary>
        private readonly string secondName;

        /// <summary>
        /// The last name.
        /// </summary>
        private readonly string lastName;

        /// <summary>
        /// The teachers.
        /// </summary>
        private readonly Person[] teachers;

        /// <summary>
        /// Initializes a new instance of the <see cref="Student"/> class.
        /// </summary>
        /// <param name="firstName">
        /// The first name.
        /// </param>
        /// <param name="secondName">
        /// The second name.
        /// </param>
        /// <param name="lastName">
        /// The last name.
        /// </param>
        /// <param name="teachers">
        /// The teachers.
        /// </param>
        public Student(string firstName, string secondName, string lastName, Person[] teachers)
            : base(firstName, secondName, lastName)
        {
            this.firstName = firstName;
            this.secondName = secondName;
            this.lastName = lastName;
            this.teachers = teachers;
            this.Teachers = new TeacherList(this.teachers);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Student"/> class.
        /// </summary>
        /// <param name="personInfo">
        /// The person info.
        /// </param>
        public Student(string[] personInfo) : base(personInfo)
        {
            this.firstName = personInfo[0];
            this.secondName = personInfo[1];
            this.lastName = personInfo[2];
            this.teachers = new Person[0];
            this.Teachers = new TeacherList(this.teachers);
        }

        /// <summary>
        /// Gets or sets the teachers.
        /// </summary>
        public TeacherList Teachers { get; set; }
    }
}