﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentList.cs" company="Serhiy Chepur">
//   2014. Serhiy Chepur@MidlleGroup
// </copyright>
// <summary>
//   The student list.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.LectionThree.TaskOne
{
    using System;

    /// <summary>
    /// The student list.
    /// </summary>
    public class StudentList : PersonList
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StudentList"/> class.
        /// </summary>
        /// <param name="persons">
        /// The persons.
        /// </param>
        public StudentList(Person[] persons)
            : base(persons)
        {
        }

        /// <summary>
        /// Gets the random student.
        /// </summary>
        public Student RandomStudent
        {
            get
            {
                var random = new Random();
                return (Student)base[random.Next(0, Length - 1)] ?? null;
            }
        }

    }
}