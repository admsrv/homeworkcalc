﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Teacher.cs" company="Serhiy Chepur">
//   2014. Serhiy Chepur@MidlleGroup
// </copyright>
// <summary>
//   Defines the Teacher type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.LectionThree.TaskOne
{
    /// <summary>
    /// The teacher.
    /// </summary>
    public class Teacher : Person
    {
        /// <summary>
        /// The students.
        /// </summary>
        private readonly Person[] students;

        /// <summary>
        /// Initializes a new instance of the <see cref="Teacher"/> class.
        /// </summary>
        /// <param name="firstName">
        /// The first name.
        /// </param>
        /// <param name="secondName">
        /// The second name.
        /// </param>
        /// <param name="lastName">
        /// The last name.
        /// </param>
        /// <param name="students">
        /// The students.
        /// </param>
        public Teacher(string firstName, string secondName, string lastName, Person[] students)
            : base(firstName, secondName, lastName)
        {
            this.students = students;
            this.Students = new StudentList(this.students);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Teacher"/> class.
        /// </summary>
        /// <param name="personInfo">
        /// The person info.
        /// </param>
        public Teacher(string[] personInfo) : base(personInfo)
        {
            this.students = new Person[0];
            this.Students = new StudentList(this.students);
        }

        /// <summary>
        /// Gets or sets the persons.
        /// </summary>
        public StudentList Students { get; set; }
    }
}