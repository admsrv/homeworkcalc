﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeacherList.cs" company="Serhiy Chepur">
//   2014. Serhiy Chepur@MidlleGroup
// </copyright>
// <summary>
//   The teacher list.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.LectionThree.TaskOne
{
    using System;

    /// <summary>
    /// The teacher list.
    /// </summary>
    public class TeacherList : PersonList
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TeacherList"/> class.
        /// </summary>
        /// <param name="persons">
        /// The persons.
        /// </param>
        public TeacherList(Person[] persons)
            : base(persons)
        {
        }

        /// <summary>
        /// Gets the random teacher.
        /// </summary>
        public Teacher RandomTeacher
        {
            get
            {
               var random = new Random();
               return (Teacher)base[random.Next(0, Length - 1)] ?? null;
            }
        }
    }
}