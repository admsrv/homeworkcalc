﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Serhiy Chepur">
//  2014. S.Chepur@MidlleGroup 
// </copyright>
// <summary>
//   The console calc.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc
{
    using System;
    using System.Globalization;
    using System.Threading;
    using System.Resources;

    using HomeWorkCalc.LectionThree.TaskOne;

    using TaskTwo;

    using HomeWorkCalc.Base;
    using HomeWorkCalc.Resources;

    /// <summary>
    /// The console calc.
    /// </summary>
   public class Program
    {
        /// <summary>
        /// The resource manager.
        /// </summary>
        private ResourceManager resourceManager;

        /// <summary>
        /// The conole main function.
        /// </summary>
        public static void Main()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru-RU");
            
            ConsoleMenu();
        }

        /// <summary>
        /// The console menu.
        /// </summary>
        private static void ConsoleMenu()
        {
            Console.Clear();
            Console.WriteLine("1:\t Calc\n2:\t String trim and sort\n3:\t Get real nums from strings\n" +
                              "4:\t Convert first chars to upper\n5:\t Get bytes from string(string to byte array)\n"+
                              "6:\t Demonstrate Person-Student-Teacher hierarchy\n"+
                              "q:\t Quit");
            var userChose = Console.ReadLine();
            switch (userChose)
            {
                case "1":
                    {
                        Console.Clear();
                        Console.WriteLine("Calc:\nUse \'Space\' as symbol delimiter\nPlease, enter the equation");
                        var mathEq = Console.ReadLine();
                        var managerObj = new Manager(mathEq, Client.Console);
                        managerObj.DoCalc();
                        break;    
                    }

                case "2":
                    {
                        Console.Clear();
                        Console.WriteLine("Please enter a string and chose data to display\n1:\tSort by alphabet\n2:\tSort by length");
                        var tmpStrings = Console.ReadLine();
                        var trimAndComp = new StringSplitAndSort(tmpStrings);
                        var dataToDisp = Console.ReadLine();
                        switch (dataToDisp)
                        {
                            case "1":
                                {
                                    Console.WriteLine(trimAndComp.SortedStrings);
                                    break;
                                }

                            case "2":
                                {
                                    Console.WriteLine(trimAndComp.SortedStringsByLength);
                                    break;
                                }
                        }

                        break;
                    }

                case "3":
                    {
                        Console.Clear();
                        Console.WriteLine("Please enter numbers or chars for demo 3\n");
                        var manager = new Manager();
                        manager.DoRealNumbers();
                        break;
                    }

                case "4":
                    {
                        Console.Clear();
                        Console.WriteLine("Please enter numbers or chars for demo 4\n");
                        var manager = new Manager();
                        manager.DoConvertToUpperFirstChars();
                        break;
                    }

                case "5":
                    {
                        Console.Clear();
                        Console.WriteLine("Please enter numbers or chars for demo 5\n");
                        var manager = new Manager();
                        manager.DoGetBytesFromString();
                        break;
                    }

                case "6":
                    {
                        Console.Clear();
                        Console.WriteLine("Please enter a persons info for demo 6\n");
                        var manager = new Manager();
                        manager.DoPersonsDemo();
                        break;
                    }

                case "q":
                    {
                        Environment.Exit(0);
                        break;
                    }
            }
        }
    }
}
