﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TaskFive.cs" company="Serhiy Chepur">
//   2014. SerhiyChepur@MidlleGroup
// </copyright>
// <summary>
//   Defines the TaskFive type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.TaskFive
{
    using System;
    using System.Text;

    /// <summary>
    /// The task five.
    /// Используя различные кодировки System.Text.Encoding, преобразовать строку в массив 
    /// байтов. Для этого использовать GetBytes соответствующей кодировки.
    /// </summary>
    public class TaskFive
    {
        private readonly Encoding defaultEncoding;

        /// <summary>
        /// The string to perform.
        /// </summary>
        private readonly string stringToPerform;

        /// <summary>
        /// The string bytes.
        /// </summary>
        private byte[] stringBytes;

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskFive"/> class.
        /// </summary>
        /// <param name="stringToPerform">
        /// The string to perform.
        /// </param>
        /// <param name="defaultEncoding">
        /// The default encoding.
        /// </param>
        public TaskFive(string stringToPerform, Encoding defaultEncoding)
        {
            if (!string.IsNullOrEmpty(stringToPerform))
            {
                this.stringToPerform = stringToPerform;
                this.defaultEncoding = defaultEncoding;
                this.stringBytes = this.StringToByteArray();
            }
            else
            {
                throw new ArgumentNullException("TaskFive: incoming string is null or empty.");
            }
        }

        /// <summary>
        /// Gets the incoming string bytes array.
        /// </summary>
        public string StringBytes
        {
            get
            {
                return BitConverter.ToString(this.stringBytes);
            }
        }

        /// <summary>
        /// The string to byte array.
        /// </summary>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        private byte[] StringToByteArray()
        {
            byte[] arrayToReturn = null;
            arrayToReturn = this.defaultEncoding.GetBytes(this.stringToPerform);
            return arrayToReturn;
        }
    }
}