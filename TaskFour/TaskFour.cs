﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TaskFour.cs" company="Serhiy Chepur">
//   2014. S.Chepur@MidlleGroup 
// </copyright>
// <summary>
//   The task four.
//   Используя тот факт, что для объекта StringBuilder доступна операция обращения по
//   индексу [] на запись, преобразовать в строке слов первую букву каждого слова в
//   заглавную.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.TaskFour
{
    using System;
    using System.Text;

    /// <summary>
    /// The task four.
    /// Используя тот факт, что для объекта StringBuilder доступна операция обращения по 
    /// индексу [] на запись, преобразовать в строке слов первую букву каждого слова в 
    /// заглавную. 
    /// </summary>
    public class TaskFour
    {
        /// <summary>
        /// The string to perform.
        /// </summary>
        private readonly string stringToPerform;

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskFour"/> class.
        /// </summary>
        /// <param name="stringToPerform">
        /// The string to perform.
        /// </param>
        public TaskFour(string stringToPerform)
         {
            if (!string.IsNullOrEmpty(stringToPerform))
            {
                this.stringToPerform = stringToPerform;
            }
            else
            {
                throw new Exception("Task four: cannot perform empty string");
            }
         }

        /// <summary>
        /// Gets the performed string.
        /// </summary>
        public string PerformedString
        {
            get
            {
                return this.FirstLattersToUpperCase();
            }
        }

        /// <summary>
        /// The first latters to upper case.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string FirstLattersToUpperCase()
        {
            var stringPerformer = new StringBuilder();
            if (!string.IsNullOrEmpty(this.stringToPerform))
            {
                var spaceBefore = false;
                var positonCursor = 0;
                stringPerformer.Append(this.stringToPerform);
                if (!string.IsNullOrWhiteSpace(stringPerformer[positonCursor].ToString()))
                {
                    stringPerformer[positonCursor] = stringPerformer[positonCursor].ToString().ToUpper().ToCharArray()[0];
                }
                else
                {
                    spaceBefore = true;
                }

                positonCursor++;
                do
                {
                    if (char.IsWhiteSpace(stringPerformer[positonCursor]))
                    {
                        spaceBefore = true;
                    }

                    if (spaceBefore)
                    {
                        if (!char.IsWhiteSpace(stringPerformer[positonCursor]))
                        {                       
                            stringPerformer[positonCursor] =
                                stringPerformer[positonCursor].ToString().ToUpper().ToCharArray()[0];
                            spaceBefore = false;
                        }
                    }

                    positonCursor++;
                }
                while (positonCursor < stringPerformer.Length);
            }

            return stringPerformer.ToString();
        }
    }
}