﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AbstractExpression.cs" company="Chepur Serhiy">
//   2014. SerhiyChepur@AltexsoftMidlleGroup
// </copyright>
// <summary>
//   The abstract expression.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.TaskOne
{
    /// <summary>
    /// The abstract expression.
    /// </summary>
    public abstract class AbstractExpression
    {
        /// <summary>
        /// The init.
        /// </summary>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public abstract dynamic Init();
    }
}
