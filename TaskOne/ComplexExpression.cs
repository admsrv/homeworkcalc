﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComplexExpression.cs" company="Chepur Serhiy">
//   2014. SerhiyChepur@AltexsoftMidlleGroup
// </copyright>
// <summary>
//   The complex expression.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace HomeWorkCalc.TaskOne
{
    using System;
    using System.Numerics;
    public class ComplexExpression : AbstractExpression
    {
        private Complex complexBase;
        public ComplexExpression(AbstractExpression real, AbstractExpression imaginary)
        {
            if (real == null)
            {

            }
            else
            {
                throw new Exception("ComplexExpression: one of operators is null or invalid");
            }
        }

        public override dynamic Init()
        {
 	        throw new NotImplementedException();
        }
        
    }
}
