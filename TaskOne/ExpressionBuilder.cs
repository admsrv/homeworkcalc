﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExpressionBuilder.cs" company="Serhuy Chepur">
//   2014. S.Chepur@MidlleGroup
// </copyright>
// <summary>
//   The expression builder.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.TaskOne
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The expression builder.
    /// </summary>
    public class ExpressionBuilder
    {
        /// <summary>
        /// The expressions tokens.
        /// </summary>
        private readonly List<string> expressionsTokens;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExpressionBuilder"/> class.
        /// </summary>
        /// <param name="expressionsTokens">
        /// The expressions tokens.
        /// </param>
        public ExpressionBuilder(List<string> expressionsTokens = null)
        {
            this.expressionsTokens = expressionsTokens;
        }

        /// <summary>
        /// The get expression.
        /// </summary>
        /// <param name="globalCounter">
        /// The global counter.
        /// </param>
        /// <returns>
        /// The <see cref="AbstractExpression"/>.
        /// </returns>
        public AbstractExpression GetExpression(ref int globalCounter)
        {
            AbstractExpression expressionToReturn = null;
            switch (this.expressionsTokens[globalCounter])
            {
                case "(":
                    {
                        var endPos = GetParametrelessEndPosition(this.expressionsTokens, globalCounter);
                        if (endPos > globalCounter)
                        {
                            var tmpstrs = new string[(endPos - globalCounter) + 1];
                            this.expressionsTokens.CopyTo(globalCounter, tmpstrs, 0, (endPos - globalCounter) + 1);
                            var parametrelessBlock = new List<string>(tmpstrs);
                            var parametrelessExpression = new ParenthesesBracketsExpression(parametrelessBlock);
                            expressionToReturn = parametrelessExpression;
                            globalCounter = endPos;
                        }
                        else
                        {
                            throw new Exception("Syntax error: Invalid count of closing parametreless brackets \')\'");
                        }

                        break;
                    }

                case "*":
                    {
                        expressionToReturn = new MultiplicationExpression();
                        break;
                    }

                case "/":
                    {
                        expressionToReturn = new DivisionExpression();
                        break;
                    }

                case "+":
                    {
                        expressionToReturn = new PlusExpression();
                        break;
                    }

                case "-":
                    {
                        expressionToReturn = new MinusExpression();
                        break;
                    }

                default:
                    {
                        var numericExpression = new NumericExpression(this.expressionsTokens[globalCounter]);
                        expressionToReturn = numericExpression;
                        break;
                    }
            }

            return expressionToReturn;
        }

        /// <summary>
        /// The get result by expression stack.
        /// </summary>
        /// <param name="expressions">
        /// The expressions.
        /// </param>
        /// <returns>
        /// The <see cref="AbstractExpression"/>.
        /// </returns>
        public AbstractExpression GetResultByExpressionStack(List<AbstractExpression> expressions)
        {
            AbstractExpression expressionToReturn = null;
            if (expressions.Count == 1 && (expressions[0].GetType() == typeof(ParenthesesBracketsExpression)))
            {
                expressionToReturn = expressions[0];
            }
            else
            {
                var polishStack = this.GetPolishNotationStack(expressions);
                expressionToReturn = this.GetAnswerByPolishStack(polishStack);                
            }

            return expressionToReturn;
        }

        /// <summary>
        /// The get answer (math expression result) by polish stack.
        /// </summary>
        /// <param name="polishStack">
        /// The math expression tokens stack by polish notation.
        /// </param>
        /// <returns>
        /// The <see cref="AbstractExpression"/>.
        /// </returns>
        private AbstractExpression GetAnswerByPolishStack(Stack<AbstractExpression> polishStack)
        {
            AbstractExpression leftPart = null;
            var outputStack = new Stack<AbstractExpression>();
            var tokenListByPolishNotation = new List<AbstractExpression>(polishStack.ToArray());
            var counter = tokenListByPolishNotation.Count - 1;
            while (tokenListByPolishNotation.Count != 0)
            {
                var tmpExp = tokenListByPolishNotation[counter];
                tokenListByPolishNotation.RemoveAt(counter);
                if (this.IsNumeric(tmpExp) || this.IsBrackets(tmpExp))
                {
                    outputStack.Push(tmpExp);
                }
                else
                {
                    var tmpRight = outputStack.Pop();
                    var tmpLeft = outputStack.Pop();
                    outputStack.Push(this.GetOperator(tmpLeft, tmpRight, tmpExp));
                }

                counter--;
            }

            leftPart = outputStack.Pop();
            return leftPart;
        }
        
        /// <summary>
        /// The get polish notation stack by list of math expression tokens.
        /// </summary>
        /// <param name="expressions">
        /// The expressions.
        /// </param>
        /// <returns>
        /// The <see cref="Stack<AbstractExpression>"/>.
        /// </returns>
        public Stack<AbstractExpression> GetPolishNotationStack(List<AbstractExpression> expressions)
        {
            var expOperatorStack = new Stack<AbstractExpression>();
            var expOutputStack = new Stack<AbstractExpression>();
            if (expressions.Count != 0 && (!this.IsBrackets(expressions[expressions.Count - 1]) && !this.IsNumeric(expressions[expressions.Count - 1])))
            {
                throw new Exception("Not finished math expression");
            }
            foreach (var expression in expressions)
            {
                if (this.IsNumeric(expression) || this.IsBrackets(expression))
                {
                    expOutputStack.Push(expression);                    
                }
                else
                {
                    if (expOperatorStack.Count == 0)
                    {
                        expOperatorStack.Push(expression);
                    }
                    else
                    {
                        if (this.CanIAddOperatorToCurrentStack(expOperatorStack.Peek(), expression))
                        {
                            expOperatorStack.Push(expression);
                        }
                        else
                        {
                            while (!this.CanIAddOperatorToCurrentStack(expOperatorStack.Peek(), expression))
                            {
                                expOutputStack.Push(expOperatorStack.Pop());
                                if (expOperatorStack.Count == 0)
                                {
                                    break;
                                }
                            }

                            expOperatorStack.Push(expression);
                        }
                    }
                }
            }

            while (expOperatorStack.Count != 0)
            {
                expOutputStack.Push(expOperatorStack.Pop());
            }

            return expOutputStack;
        }

        /// <summary>
        /// The get parametreless end position.
        /// </summary>
        /// <param name="expressionStringTokens">
        /// The expression tokens strings.
        /// </param>
        /// <param name="globalCounter">
        /// The global counter.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private static int GetParametrelessEndPosition(List<string> expressionStringTokens, int globalCounter)
        {
            var openParametrelessCount = 1;
            var closeParametrelessCount = 0;
            var openParametrelessEndPos = globalCounter;
            for (var localCounter = globalCounter + 1; localCounter < expressionStringTokens.Count; localCounter++)
            {
                if (expressionStringTokens[localCounter].Equals("("))
                {
                    if (closeParametrelessCount < openParametrelessCount)
                    {
                        openParametrelessCount++;
                    }
                }

                if (expressionStringTokens[localCounter].Equals(")"))
                {
                    if (closeParametrelessCount < openParametrelessCount)
                    {
                        closeParametrelessCount++;
                    }
                }

                if (openParametrelessCount == closeParametrelessCount)
                {
                    openParametrelessEndPos = localCounter;
                    break;
                }
            }

            return openParametrelessEndPos;
        }

        /// <summary>
        /// Gets the math expression by operator and expression operands.
        /// </summary>
        /// <param name="leftPart">
        /// The left part of math expression.
        /// </param>
        /// <param name="rightPart">
        /// The right part of math expression.
        /// </param>
        /// <param name="operatorE">
        /// The math expression operator.
        /// </param>
        /// <returns>
        /// The <see cref="AbstractExpression"/>.
        /// </returns>
        private AbstractExpression GetOperator(
            AbstractExpression leftPart, AbstractExpression rightPart, AbstractExpression operatorE)
        {
            var tmpOperatorType = operatorE.GetType();
            AbstractExpression toReturn = null;
            if (tmpOperatorType == typeof(MinusExpression))
            {
                toReturn = new MinusExpression(leftPart, rightPart);
            }

            if (tmpOperatorType == typeof(PlusExpression))
            {
                toReturn = new PlusExpression(leftPart, rightPart);
            }

            if (tmpOperatorType == typeof(MultiplicationExpression))
            {
                toReturn = new MultiplicationExpression(leftPart, rightPart);
            }

            if (tmpOperatorType == typeof(DivisionExpression))
            {
                toReturn = new DivisionExpression(leftPart, rightPart);
            }

            return toReturn;
        }

        /// <summary>
        /// The can i add operator to current stack?
        /// </summary>
        /// <param name="expressionOperatorInStack">
        /// The expression operator in stack.
        /// </param>
        /// <param name="currentOperator">
        /// The current operator.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool CanIAddOperatorToCurrentStack(AbstractExpression expressionOperatorInStack, AbstractExpression currentOperator)
        {
            var answer = false;
            switch (this.IsHighPriority(expressionOperatorInStack))
            {
                case false:
                    {
                        answer = !this.IsLowPriority(currentOperator); 
                        break;
                    }
            }

            return answer;
        }

        /// <summary>
        /// Check if operation have low precedence priority.
        /// </summary>
        /// <param name="operationExpression">
        /// The operation expression.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool IsLowPriority(AbstractExpression operationExpression)
        {
            return operationExpression.GetType() == typeof(PlusExpression) || operationExpression.GetType() == typeof(MinusExpression);
        }

        /// <summary>
        /// Check if operation have high precedence priority.
        /// </summary>
        /// <param name="operationExpression">
        /// The operation expression.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool IsHighPriority(AbstractExpression operationExpression)
        {
            return operationExpression.GetType() == typeof(MultiplicationExpression) || operationExpression.GetType() == typeof(DivisionExpression);
        }

        /// <summary>
        /// Check if token is numeric.
        /// </summary>
        /// <param name="operationExpression">
        /// The operation expression.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool IsNumeric(AbstractExpression operationExpression)
        {
            return operationExpression.GetType() == typeof(NumericExpression); 
        }

        /// <summary>
        /// Check if token is brackets.
        /// </summary>
        /// <param name="operationExpression">
        /// The operation expression.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool IsBrackets(AbstractExpression operationExpression)
        {
            return operationExpression.GetType() == typeof(ParenthesesBracketsExpression);
        } 
    }
}
