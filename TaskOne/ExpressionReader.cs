﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExpressionReader.cs" company="Serhiy Chepur">
//   2014. S.Chepur@MidlleGroup
// </copyright>
// <summary>
//   The math expression's reader.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.TaskOne
{
    using System.Collections.Generic;

    /// <summary>
    /// The math expression reader.
    /// </summary>
    public class ExpressionReader
    {
        /// <summary>
        /// The expression strings.
        /// </summary>
        private readonly List<string> expressionStrings;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExpressionReader"/> class.
        /// </summary>
        /// <param name="expressionStrings">
        /// The expression strings.
        /// </param>
        public ExpressionReader(List<string> expressionStrings)
        {
            this.expressionStrings = expressionStrings;
        }

        /// <summary>
        /// Gets the math expressions list.
        /// </summary>
        public AbstractExpression MathExpression
        {
            get
            {
                return this.ReadAllExpressions(this.expressionStrings);
            }
        }

        /// <summary>
        /// The read all expressions.
        /// </summary>
        /// <param name="expressionStrs">
        /// The expression strs.
        /// </param>
        /// <returns>
        /// The <see cref="AbstractExpression"/>.
        /// </returns>
        private AbstractExpression ReadAllExpressions(List<string> expressionStrs)
        {
            AbstractExpression expressionToReturn = null;
            var expressionsStack = new List<AbstractExpression>();
            var expBuilder = new ExpressionBuilder(expressionStrs);
            for (var globalCounter = 0; globalCounter < expressionStrs.Count; globalCounter++)
            {
                var tmpExpression = expBuilder.GetExpression(ref globalCounter);
                expressionsStack.Add(tmpExpression);
            }

            expressionToReturn = expBuilder.GetResultByExpressionStack(expressionsStack);
            return expressionToReturn;
        }
    }
}
