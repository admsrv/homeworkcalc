﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MultiplicationExpression.cs" company="Serhiy Chepur">
//   2014. S.Chepur@MidlleGroup
// </copyright>
// <summary>
//   Defines the MultiplicationExpression type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.TaskOne
{
    /// <summary>
    /// The multiplication expression.
    /// </summary>
    public class MultiplicationExpression : AbstractExpression
    {
        /// <summary>
        /// The expression left part.
        /// </summary>
        private AbstractExpression expressionLeftPart;

        /// <summary>
        /// The expression right part.
        /// </summary>
        private AbstractExpression expressionRightPart;

        /// <summary>
        /// Initializes a new instance of the <see cref="MultiplicationExpression"/> class.
        /// </summary>
        /// <param name="expressionLeftPart">
        /// The expression left part.
        /// </param>
        /// <param name="expressionRightPart">
        /// The expression right part.
        /// </param>
        public MultiplicationExpression(AbstractExpression expressionLeftPart, AbstractExpression expressionRightPart)
        {
            this.expressionLeftPart = expressionLeftPart;
            this.expressionRightPart = expressionRightPart;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MultiplicationExpression"/> class.
        /// </summary>
        public MultiplicationExpression()
        {
            this.expressionLeftPart = null;
            this.expressionRightPart = null;
        }

        /// <summary>
        /// Gets or sets the expression left part.
        /// </summary>
        public AbstractExpression ExpressionLeftPart
        {
            get
            {
                return this.expressionLeftPart;
            }

            set
            {
                this.expressionLeftPart = value;
            }
        }

        /// <summary>
        /// Gets or sets the expression right part.
        /// </summary>
        public AbstractExpression ExpressionRightPart
        {
            get
            {
                return this.expressionRightPart;
            }

            set
            {
                this.expressionRightPart = value;
            }
        }

        /// <summary>
        /// The init.
        /// </summary>
        /// <returns>
        /// The <see cref="dynamic"/>.
        /// </returns>
        public override dynamic Init()
        {
            var left = (decimal)this.expressionLeftPart.Init();
            var right = (decimal)this.expressionRightPart.Init();
            var result = left * right;
            return result;
        }
    }
}
