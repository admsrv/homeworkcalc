﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NumericExpression.cs" company="Serhiy Chepur">
//   2014. S.Chepur@MidlleGroup
// </copyright>
// <summary>
//   The numeric expression.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.TaskOne
{
    using System;

    /// <summary>
    /// The numeric expression.
    /// </summary>
    public class NumericExpression : AbstractExpression
    {
        /// <summary>
        /// The token string.
        /// </summary>
        private readonly string tokenString;

        /// <summary>
        /// Initializes a new instance of the <see cref="NumericExpression"/> class.
        /// </summary>
        /// <param name="tokenString">
        /// The expression token string.
        /// </param>
        public NumericExpression(string tokenString)
        {
            this.tokenString = tokenString;
        }

        /// <summary>
        /// The init.
        /// </summary>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public override dynamic Init()
        {
            object numericValue = null;
            if (this.tokenString.Contains(",") || this.tokenString.Contains("."))
            {
                double doubleVariant;
                float floatVariant;
                if(double.TryParse(this.tokenString, out doubleVariant))
                {
                    numericValue = doubleVariant;
                }

                if (float.TryParse(this.tokenString, out floatVariant))
                {
                    numericValue = floatVariant;
                }
            }
            else
            {
                int intVariant;
                if (int.TryParse(this.tokenString, out intVariant))
                {
                    numericValue = intVariant;
                }
                else
                {
                    throw new Exception("Invalid numeric token");
                }
            }

            return numericValue;
        }
    }
}
