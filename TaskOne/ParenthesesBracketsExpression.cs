﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ParenthesesBracketsExpression.cs" company="Serhiy Chepur">
//   2014. S.Chepur@MidlleGroup
// </copyright>
// <summary>
//   Defines the ParenthesesBracketsExpression type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.TaskOne
{
    using System.Collections.Generic;

    /// <summary>
    /// The parentheses brackets expression.
    /// </summary>
    public class ParenthesesBracketsExpression : AbstractExpression
    {
        /// <summary>
        /// The paretheses tokens.
        /// </summary>
        private readonly List<string> parethesesTokens;

        /// <summary>
        /// The local expressions stack.
        /// </summary>
        private List<AbstractExpression> localExpressionsStack;

        /// <summary>
        /// The parentheses expression.
        /// </summary>
        private AbstractExpression parenthesesExpression;

        /// <summary>
        /// Initializes a new instance of the <see cref="ParenthesesBracketsExpression"/> class.
        /// </summary>
        /// <param name="parethesesTokens">
        /// The paretheses tokens.
        /// </param>
        public ParenthesesBracketsExpression(List<string> parethesesTokens)
        {
            this.parethesesTokens = parethesesTokens;
            this.localExpressionsStack = null;
            this.parenthesesExpression = this.GetParenthesesBracketsExpression();
        }

        /// <summary>
        /// Gets the members stack.
        /// </summary>
        public List<AbstractExpression> MembersStack
        {
            get
            {
                return this.localExpressionsStack;
            }
        }

        /// <summary>
        /// The init.
        /// </summary>
        /// <returns>
        /// The <see cref="dynamic"/>.
        /// </returns>
        public override dynamic Init()
        {
            var expressionToReturn = this.parenthesesExpression;
            return expressionToReturn.Init();
        }

        /// <summary>
        /// The get parentheses brackets expression.
        /// </summary>
        /// <returns>
        /// The <see cref="AbstractExpression"/>.
        /// </returns>
        private AbstractExpression GetParenthesesBracketsExpression()
        {
            AbstractExpression expressionToReturn = null;
            var expressionsStack = new List<AbstractExpression>();
            var expBuilder = new ExpressionBuilder(this.parethesesTokens);
            for (var globalCounter = 1; globalCounter < this.parethesesTokens.Count - 1; globalCounter++)
            {
                var tmpExpression = expBuilder.GetExpression(ref globalCounter);
                expressionsStack.Add(tmpExpression);
            }

            expressionToReturn = expBuilder.GetResultByExpressionStack(expressionsStack);
            return expressionToReturn;
        }
    }
}
