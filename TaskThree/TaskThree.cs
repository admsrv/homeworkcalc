﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TaskThree.cs" company="Serhiy Chepur">
//   2014. S.Chepur@MidlleGroup
// </copyright>
// <summary>
//   Defines the TaskThree type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.TaskThree
{
    using System;
    using System.Text;

    /// <summary>
    /// The task three.
    /// 3. Вводятся вещественные числа (возможно, на нескольких строках), конец ввода - 0. На 
    /// одной строке может быть несколько вещественных чисел, а также не числа. Используя
    /// метод Append класса StringBuilder, сформировать строку, в которой эти вещественные 
    /// числа разделены одним пробелом, и количество цифр в дробной части равно 1. Не числа -
    /// игнорировать. Для разбора вещественного использовать double.TryParse, для 
    /// форматирования вещественного - d.ToString (формат вывода для вещественного с 1 
    /// цифрой в дробной части) 
    /// </summary>
    public class TaskThree
    {
        /// <summary>
        /// The array to perform.
        /// </summary>
        private readonly string[] arrayToPerform;

        /// <summary>
        /// The performed string.
        /// </summary>
        private readonly string performedString;

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskThree"/> class.
        /// </summary>
        /// <param name="arrayToPerform">
        /// The array to perform.
        /// </param>
        public TaskThree(string[] arrayToPerform)
        {
            if (arrayToPerform != null)
            {
                if (arrayToPerform.Length == 0)
                {
                    throw new Exception("TaskThree three: Empty array");
                }

                this.arrayToPerform = arrayToPerform;
                this.performedString = this.Perform();
            }
        }

        /// <summary>
        /// Gets the performed array to string.
        /// </summary>
        public string PerformedArrayToString
        {
            get 
            {
                return this.performedString;
            }
        }

        /// <summary>
        /// The perform.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string Perform()
        {
            string stringToReturn = null;
            var worker = new StringBuilder();
            var splitChars = new char[] { ' ' };
            foreach (string line in this.arrayToPerform)
            {
                var words = line.Split(splitChars, StringSplitOptions.RemoveEmptyEntries);
                var numbersString = this.GetNums(words);
                if (!string.IsNullOrEmpty(numbersString))
                {
                    worker.Append(numbersString);
                }
            }

            stringToReturn = worker.ToString();
            return stringToReturn;
        }

        /// <summary>
        /// The get nums.
        /// </summary>
        /// <param name="words">
        /// The words.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetNums(string[] words)
        {
            var stringBuilder = new StringBuilder();
            var formater = System.Threading.Thread.CurrentThread.CurrentCulture;
            formater.NumberFormat.CurrencyDecimalDigits = 1;
            formater.NumberFormat.NumberDecimalDigits = 1;
            string stringToReturn = null;
            foreach (string word in words)
            {
                double num;
                if (double.TryParse(word, System.Globalization.NumberStyles.Any, formater, out num))
                {
                    stringBuilder.AppendFormat("{0} ", num.ToString("F", formater));
                }
            }

            stringToReturn = stringBuilder.ToString();
            return stringToReturn;
        }
    }
}
