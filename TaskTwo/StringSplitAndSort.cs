﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringSplitAndSort.cs" company="Serhiy Chepur">
//   2014. S.Chepur@MidlleGroup
// </copyright>
// <summary>
//   Defines the StringSplitAndSort type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace HomeWorkCalc.TaskTwo
{
    using System;

    /// <summary>
    /// The string split and sort.
    /// </summary>
    public class StringSplitAndSort
    {
        /// <summary>
        /// The string to perform.
        /// </summary>
        private readonly string stringToPerform;

        /// <summary>
        /// Initializes a new instance of the <see cref="StringSplitAndSort"/> class.
        /// </summary>
        /// <param name="stringToPerform">
        /// The string to perform.
        /// </param>
        public StringSplitAndSort(string stringToPerform)
         {
            if (!string.IsNullOrEmpty(stringToPerform))
            {
             this.stringToPerform = stringToPerform;
            }
            else
            {
                throw new Exception();
            }
         }

        /// <summary>
        /// Gets the sorted strings.
        /// </summary>
        public string SortedStrings
        {
            get
            {
                return this.SplitAndSort();
            }
        }

        /// <summary>
        /// Gets the sorted strings.
        /// </summary>
        public string SortedStringsByLength
        {
            get
            {
                string[] abc = { " " };
                var tmpStr = this.stringToPerform.Split(abc, StringSplitOptions.RemoveEmptyEntries);
                this.SortByLength(tmpStr);
                var splitedAndSortedStr = string.Join(" ", tmpStr);
                return splitedAndSortedStr;
            }
        }

        /// <summary>
        /// The split and sort.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string SplitAndSort()
        {
            string[] abc = { " " };
            var tmpStr = this.stringToPerform.Split(abc, StringSplitOptions.RemoveEmptyEntries);
            this.SortByAbc(tmpStr);
            var splitedAndSortedStr = string.Join(" ", tmpStr);
            return splitedAndSortedStr;
        }

        /// <summary>
        /// The compare by length.
        /// </summary>
        /// <param name="left">
        /// The left.
        /// </param>
        /// <param name="right">
        /// The right.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private int CompareByLength(string left, string right)
            {
                if (left.Length < right.Length)
                {
                    return 1;
                }

                if (left.Length > right.Length)
                {
                    return -1;
                }

                if (left.Length == right.Length)
                {
                    return 0;
                }

                return 0;
            }

        /// <summary>
        /// The sort by length.
        /// </summary>
        /// <param name="stringToSort">
        /// The string to sort.
        /// </param>
        private void SortByLength(string[] stringToSort)
        {
            Array.Sort(stringToSort, this.CompareByLength);
        }
     
        /// <summary>
        /// The sort by abc.
        /// </summary>
        /// <param name="stringToSort">
        /// The string to sort.
        /// </param>
        private void SortByAbc(string[] stringToSort)
        {
            Array.Sort(stringToSort);
        }
    }
}