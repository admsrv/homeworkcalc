﻿namespace WpfCalc
{
    using System.Collections.Generic;
    using System.Text;
    using System.Windows.Input;
    using HomeWorkCalc.TaskOne;

    public class CalcManager
    {
        public CalcManager()
        {
            this.UserInput = new Stack<string>();
        }
        #region Keys
        public ICommand PressedKeyOne
        {
            get
            {
                return null;
            }
            set
            {
                System.Windows.MessageBox.Show("s");
                this.UserInput.Push("1");
            }
        }
        public ICommand PressedKeyTwo
        {
            get
            {
                return null;
            }

            set
            {
                this.UserInput.Push("2");
            }
        }
        public ICommand PressedKeyThree
        {
            get
            {
                return null;
            }

            set
            {
                this.UserInput.Push("3");
            }
        }
        public ICommand PressedKeyFour
        {
            get
            {
                return null;
            }

            set
            {
                this.UserInput.Push("4");
            }
        }
        public ICommand PressedKeyFive
        {
            get
            {
                return null;
            }

            set
            {
                this.UserInput.Push("5");
            }
        }
        public ICommand PressedKeySix
        {
            get
            {
                return null;
            }

            set
            {
                this.UserInput.Push("6");
            }
        }
        public ICommand PressedKeySeven
        {
            get
            {
                return null;
            }

            set
            {
                this.UserInput.Push("7");
            }
        }
        public ICommand PressedKeyEight
        {
            get
            {
                return null;
            }

            set
            {
                this.UserInput.Push("8");
            }
        }
        public ICommand PressedKeyNine
        {
            get
            {
                return null;
            }

            set
            {
                this.UserInput.Push("9");
            }
        }
        public ICommand PressedKeyZero
        {
            get
            {
                return null;
            }

            set
            {
                this.UserInput.Push("0");
            }
        }
        #endregion
        private Stack<string> UserInput { get; set; }

        public string Result
        {
            get
            {
                var expressions = this.UserInput.ToArray();
                return expressions.Length > 0 ? string.Join(string.Empty, expressions) : "0";
            }
        }
    }
}