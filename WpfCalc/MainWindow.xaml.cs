﻿namespace WpfCalc
{
    using System.Globalization;
    using System.Threading;
    using System.Windows;
    using System.Windows.Input;

    using WpfCalc.ViewModels;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {           
            InitializeComponent();
            var tThread = CultureInfo.CreateSpecificCulture("ru-RU"); 
            tThread.NumberFormat.NumberDecimalSeparator = ".";
            tThread.NumberFormat.CurrencyDecimalSeparator = ".";
            tThread.NumberFormat.PercentDecimalSeparator = ".";
            Thread.CurrentThread.CurrentCulture = tThread;

            this.VM = new MainWindowViewModel();
            this.DataContext = VM;
        }

        private MainWindowViewModel VM;

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }
    }
}
