﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BaseViewModel.cs" company="AltexSoft midlle group">
//   Serhiy Chepur @ AltexSoft midlle group student. 2015
// </copyright>
// <summary>
//   The base MVVM view model for math calculator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WpfCalc.ViewModels
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// The base MVVM view model for math calculator.
    /// </summary>
    public class BaseViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// The property changed event raise when user try to input new token of math expression.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The notify property changed event body.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
