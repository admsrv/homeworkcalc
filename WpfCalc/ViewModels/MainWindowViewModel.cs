﻿namespace WpfCalc.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using HomeWorkCalc.TaskOne;
    using Microsoft.TeamFoundation.MVVM;

    class MainWindowViewModel : BaseViewModel
    {
        /// <summary>
        /// The math expression tokens stack.
        /// </summary>
        private readonly Stack<string> expressionStack;

        /// <summary>
        /// The math expression reader.
        /// </summary>
        private ExpressionReader mathExpressionReader;

        /// <summary>
        /// The close brackets counter.
        /// </summary>
        private int closeBracketsCount;

        /// <summary>
        /// The open brackets counter.
        /// </summary>
        private int openBracketsCount;

        /// <summary>
        /// The comma token input locked/unlocked flag.
        /// </summary>
        private bool commaLocked;

        /// <summary>
        /// Gets or sets the button click command.
        /// </summary>
        public ICommand ButtonClick { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowViewModel"/> class.
        /// </summary>
        public MainWindowViewModel()
        {
            this.expressionStack = new Stack<string>();
            this.openBracketsCount = this.closeBracketsCount = 0;
            this.commaLocked = false;
            this.ButtonClick = new RelayCommand(this.ButtonClicked);
        }

        /// <summary>
        /// Gets or sets the expression result.
        /// </summary>
        public string ExpressionResult
        {
            get
            {
                return this.GetExpressionResult();
            }

            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                this.NotifyPropertyChanged("ExpressionResult");
            }
        }

        /// <summary>
        /// Gets the expression string.
        /// </summary>
        private string ExpressionString 
        { 
            get
            {
                var mathTokensArray = this.expressionStack.ToArray();
                return mathTokensArray.Length == 0 ? "0" : string.Concat(mathTokensArray.Reverse());
            } 
        }

        /// <summary>
        /// The get math expression result by local tokens stack.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetExpressionResult()
        {
            if (this.expressionStack.Count <= 0)
            {
                return "0";
            }

            if (!this.CanPushOperationToStack())
            {
                return this.ExpressionString;
            }

            try
            {
                var tokens = this.PrepareString(this.ExpressionString);
                var expWorker = new ExpressionReader(tokens);
                var tmpExpressionResult = expWorker.MathExpression.Init();
                object convertedTmpExpressionResult = tmpExpressionResult;
                return convertedTmpExpressionResult.ToString();
            } 
            catch
            {
                return this.ExpressionString;
            }
        }

        /// <summary>
        /// The button clicked command body.
        /// </summary>
        /// <param name="parameter">
        /// The parameter.
        /// </param>
        private void ButtonClicked(object parameter)
        {
            var callerName = parameter as string;
            if (callerName != null)
            {
                if (string.IsNullOrEmpty(callerName))
                {
                    return;
                }

                var operations = new List<string>() { "Multiplication", "Division", "Plus" };
                if (operations.Contains(callerName))
                {
                    if (!this.OperatorPushCheck())
                    {
                        return;
                    }
                }

                switch (callerName)
                {
                    case "One":
                        {
                            this.expressionStack.Push("1");
                            break;
                        }   
                    case"Two":
                        {
                            this.expressionStack.Push("2");
                            break;
                        }   
                    case"Three":
                        {
                            this.expressionStack.Push("3");
                            break;
                        } 
                    case"Four":
                        {
                            this.expressionStack.Push("4");
                            break;
                        }  
                    case"Five":
                        {
                            this.expressionStack.Push("5");
                            break;
                        }  
                    case"Six":
                        {
                            this.expressionStack.Push("6");
                            break;
                        }   
                    case"Seven":
                        {
                            this.expressionStack.Push("7");
                            break;
                        } 
                    case"Eight":
                        {
                            this.expressionStack.Push("8");
                            break;
                        } 
                    case"Nine":
                        {
                            this.expressionStack.Push("9");
                            break;
                        }  
                    case"Zero":
                        {
                            this.expressionStack.Push("0");
                            break;
                        }  
                    case"Equals":
                        {
                            this.ExpressionResult = this.GetExpressionResult();
                            return;                            
                        }
                    case "Comma":
                        {
                            if (!this.CanPushOperationToStack()) return;
                            if (!this.commaLocked)
                            {
                                this.expressionStack.Push(".");
                                this.commaLocked = true;
                            }
                            break;
                        }
                       case "BracketsOn":
                        {
                            if (this.CanPushBracketToStack("("))
                            {
                                this.expressionStack.Push(" ( ");
                            }
                            break;
                        } 
                       case "BracketsOff":
                        {
                            if (this.CanPushBracketToStack(")"))
                            {
                                this.expressionStack.Push(" ) ");
                            }
                            break;
                        }
                       case "Multiplication":
                        {
                            this.expressionStack.Push(" * ");
                            break;
                        }
                       case "Division":
                        {
                            this.expressionStack.Push(" / ");
                            break;
                        }    
                      case "Minus":
                        {
                            if (this.expressionStack.Count == 0 || this.expressionStack.Peek().Contains(" + ")
                            || this.expressionStack.Peek().Contains(" - ") || this.expressionStack.Peek().Contains(" / ")
                            || this.expressionStack.Peek().Contains(" * ") || this.expressionStack.Peek().Contains(" ( "))
                            {
                                this.expressionStack.Push(" -");
                            }
                            else
                            {
                                if (this.expressionStack.Peek().Contains("."))
                                {
                                    return;
                                }

                                if (!this.OperatorPushCheck())
                                {
                                    return;
                                }
                                this.expressionStack.Push(" - ");
                            }
                            break;
                        }        
                      case "Plus":
                        {
                            this.expressionStack.Push(" + ");
                            break;
                        }
                      case "Clear":
                        {
                            this.commaLocked = false;
                            this.openBracketsCount = this.closeBracketsCount = 0;
                            this.expressionStack.Clear();
                            break;
                        }        
                    default:
                        {
                            MessageBox.Show(string.Format("Unknown Button:{0}", callerName));
                            break;
                        }
                }
                this.ExpressionResult = this.ExpressionString;
            }
        }

        /// <summary>
        /// The operator push to expression tokens stack check.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool OperatorPushCheck()
        {
            if (!this.CanPushOperationToStack())
            {
                return false;
            }

            if (this.commaLocked)
            {
                this.commaLocked = false;
            }

            if (this.openBracketsCount == this.closeBracketsCount)
            {
                this.closeBracketsCount = this.openBracketsCount = 0;
            }

            return true;
        }

        /// <summary>
        /// The can push bracket to math expression stack.
        /// </summary>
        /// <param name="bracket">
        /// The bracket.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool CanPushBracketToStack(string bracket = "")
        {
            if (this.expressionStack.Count == 0 && bracket == ")")
            {
                return false;
            }

            if (!(this.expressionStack.Count == 0 && bracket == "("))
            {
                if (this.expressionStack.Peek().Contains(" -") || this.expressionStack.Peek().Contains("."))
                {
                    return false;
                }
            }
            else
            {
                this.openBracketsCount++;
                return true; 
            }

            switch (bracket)
            {
                case "(":
                    {
                        if (!this.expressionStack.Peek().Contains(" ) "))
                        {
                            if (!this.CanPushOperationToStack())
                            {
                                this.openBracketsCount++;
                                return true;
                            }
                        }

                        break;
                    }

                case ")":
                    {
                        if (!this.CanPushOperationToStack()) return false;
                        if (this.closeBracketsCount < this.openBracketsCount)
                        {
                            this.closeBracketsCount++;
                            return true;
                        }

                        break;
                    }

                default:
                    {
                        return false;
                    }
            }

            return false;
        }

        /// <summary>
        /// The can push operation to math expression tokens stack.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool CanPushOperationToStack()
        {
            if (this.expressionStack.Count <= 0 || this.expressionStack.Peek().Contains(" -")
                || this.expressionStack.Peek().Contains(" - ") || this.expressionStack.Peek().Contains(" + ")
                || this.expressionStack.Peek().Contains(" * ") || this.expressionStack.Peek().Contains(" / ")
                || this.expressionStack.Peek().Contains(" ( ") || this.expressionStack.Peek().Contains("."))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The prepare expression string for <see cref="ExpressionReader"/>. Tokenization method.
        /// </summary>
        /// <param name="unpreparedString">
        /// The unprepared string.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        private List<string> PrepareString(string unpreparedString)
        {
            var expressionString = unpreparedString;
            var preparedTokens = new List<string> { };
            if (!string.IsNullOrEmpty(unpreparedString))
            {
                var preparedExpressionString = expressionString.ToLower();
                var abc = new[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', '\'', '"', '.' };
                preparedExpressionString = preparedExpressionString.Trim(abc);
                preparedTokens = new List<string>(preparedExpressionString.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
            }

            return preparedTokens;
        }
        
    }
}
